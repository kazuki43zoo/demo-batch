package com.example.demo.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

public class MySQLTestContainer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  private static final Logger LOGGER = LoggerFactory.getLogger(MySQLTestContainer.class);
  private static final String MYSQL_IMAGE = "mysql:5.7";
  private static final String DATABASE_NAME = "devdb";
  private static final String USERNAME = "devuser";
  private static final String PASSWORD = "devuser";
  private static final String SCHEMA_SQL = "mysql/schema.sql";
  private static final String SCHEMA_SQL_SQL_IN_CONTAINER = "/docker-entrypoint-initdb.d/schema.sql";

  private static MySQLContainer<?> mysql;

  static {
    initialize();
    start();
  }

  public static void initialize() {
    if (Boolean.parseBoolean(System.getProperty("testcontainers.enabled", "false"))) {
      if (mysql == null) {
        mysql = new MySQLContainer<>(MYSQL_IMAGE)
            .withDatabaseName(DATABASE_NAME)
            .withUsername(USERNAME)
            .withPassword(PASSWORD)
            .withLogConsumer(new Slf4jLogConsumer(LOGGER))
            .withClasspathResourceMapping(SCHEMA_SQL, SCHEMA_SQL_SQL_IN_CONTAINER, BindMode.READ_ONLY);
      }
    }
  }

  public static void start() {
    if (mysql != null && !mysql.isRunning()) {
      mysql.start();
    }
  }

  public static void stop() {
    if (mysql != null && mysql.isRunning()) {
      mysql.stop();
      mysql = null;
    }
  }

  public static void exportJdbcUrlToSystemProperty() {
    if (mysql != null && mysql.isRunning()) {
      System.setProperty("spring.datasource.url", mysql.getJdbcUrl());
    }
  }

  public static void clearJdbcUrlFromSystemProperty() {
    System.clearProperty("spring.datasource.url");
  }

  public static String getJdbcUrl() {
    return mysql == null ? null : mysql.getJdbcUrl();
  }

  @Override
  public void initialize(ConfigurableApplicationContext context) {
    if (mysql != null && mysql.isRunning()) {
      String mysqlJdbcUrl = mysql.getJdbcUrl();
      TestPropertyValues.of("spring.datasource.url=" + mysqlJdbcUrl)
          .applyTo(context.getEnvironment());
    }
  }
}