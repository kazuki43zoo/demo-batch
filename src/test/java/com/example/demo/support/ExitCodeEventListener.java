package com.example.demo.support;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.ExitCodeEvent;
import org.springframework.context.ApplicationListener;

public class ExitCodeEventListener implements ApplicationListener<ExitCodeEvent>, InitializingBean {

  public static ExitCodeEvent event;

  public static boolean isNormal() {
    return event == null || event.getExitCode() == 0;
  }

  @Override
  public void onApplicationEvent(ExitCodeEvent event) {
    ExitCodeEventListener.event = event;
  }

  @Override
  public void afterPropertiesSet() {
    event = null;
  }

}
