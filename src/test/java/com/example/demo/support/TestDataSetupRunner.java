package com.example.demo.support;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Optional;

public class TestDataSetupRunner implements ApplicationRunner, Ordered {
  private final PlatformTransactionManager transactionManager;
  private final ApplicationRunner delegate;

  private TransactionStatus transactionStatus;

  public TestDataSetupRunner(ApplicationRunner delegate) {
    this.transactionManager = null;
    this.delegate = delegate;
  }

  public TestDataSetupRunner(PlatformTransactionManager transactionManager, ApplicationRunner delegate) {
    this.transactionManager = transactionManager;
    this.delegate = delegate;
  }

  public void beginTransaction() {
    Optional.ofNullable(transactionManager).ifPresent(x -> {
      DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
      definition.setName("SetupApplicationRunner");
      this.transactionStatus = x.getTransaction(definition);
    });
  }

  public void rollbackTransaction() {
    Optional.ofNullable(transactionStatus).ifPresent(s -> Optional.ofNullable(transactionManager)
        .ifPresent(m -> m.rollback(s)));
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    beginTransaction();
    try {
      delegate.run(args);
    } catch (Throwable e) {
      rollbackTransaction();
      throw e;
    }
  }

  @Override
  public int getOrder() {
    return Ordered.HIGHEST_PRECEDENCE;
  }

}

