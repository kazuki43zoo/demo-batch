package com.example.demo.support;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Function;

public class MockOperations<I, O> {

  private final Queue<Function<I, O>> operations = new LinkedList<>();
  private final Queue<I> inputs = new LinkedList<>();

  public MockOperations<I, O> add(Function<I, O> operation) {
    operations.offer(operation);
    return this;
  }

  public I pollInput() {
    return inputs.poll();
  }

  public List<I> getAllInput() {
    return new LinkedList<>(inputs);
  }

  public boolean hasInputs() {
    return !inputs.isEmpty();
  }

  public void clear() {
    operations.clear();
    inputs.clear();
  }

  public O execute(I input) {
    inputs.offer(input);
    return Optional.ofNullable(operations.poll()).orElseThrow(()
        -> new IllegalStateException("Operation is empty.")).apply(input);
  }
}
