package com.example.demo.support.grpc;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class MockGrpcServer {
  private static final Logger logger = LoggerFactory.getLogger(MockGrpcServer.class);
  private final int port;
  private final BindableService[] services;
  private String portExportPropertyKey;
  private Server server;

  public MockGrpcServer(int port, BindableService... services) {
    this.port = port;
    this.services = services;
  }

  public MockGrpcServer(BindableService... services) {
    this(50051, services);
  }

  public void setPortExportPropertyKey(String portExportPropertyKey) {
    this.portExportPropertyKey = portExportPropertyKey;
  }

  public void start() throws IOException {
    ServerBuilder<?> builder = ServerBuilder.forPort(port);
    Stream.of(services).forEach(builder::addService);
    server = builder
        .build()
        .start();
    Optional.ofNullable(portExportPropertyKey)
        .ifPresent(x -> System.setProperty(x, String.valueOf(getPort())));
    logger.info("Server started, listening on " + getPort());
    Runtime.getRuntime().addShutdownHook(new Thread(MockGrpcServer.this::stop));
  }

  public int getPort() {
    return server == null ? -1 : server.getPort();
  }

  public void stop() {
    if (server != null) {
      Optional.ofNullable(portExportPropertyKey).ifPresent(System::clearProperty);
      int port = getPort();
      try {
        server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        e.printStackTrace(System.err);
      }
      logger.info("Server stopped, listening on " + port);
      server = null;
    }
  }
}

