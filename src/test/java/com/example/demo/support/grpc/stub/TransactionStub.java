package com.example.demo.support.grpc.stub;

import com.example.demo.support.MockOperations;
import com.example.demo.proto.TransactionGrpc;
import com.example.demo.proto.TransactionReferReply;
import com.example.demo.proto.TransactionReferRequest;
import com.example.demo.proto.TransactionReply;
import com.example.demo.proto.TransactionRequest;
import io.grpc.stub.StreamObserver;

public class TransactionStub extends TransactionGrpc.TransactionImplBase {

  public static final TransactionStub INSTANCE = new TransactionStub();

  public final MockOperations<TransactionRequest, TransactionReply> mockOperationsForCreate
      = new MockOperations<>();
  public final MockOperations<TransactionReferRequest, TransactionReferReply> mockOperationsForRefer
      = new MockOperations<>();

  private TransactionStub() {
    // NOP
  }

  public void clear() {
    mockOperationsForCreate.clear();
    mockOperationsForRefer.clear();
  }

  @Override
  public void create(TransactionRequest request, StreamObserver<TransactionReply> responseObserver) {
    responseObserver.onNext(mockOperationsForCreate.execute(request));
    responseObserver.onCompleted();
  }

  @Override
  public void refer(TransactionReferRequest request, StreamObserver<TransactionReferReply> responseObserver) {
    responseObserver.onNext(mockOperationsForRefer.execute(request));
    responseObserver.onCompleted();
  }

}
