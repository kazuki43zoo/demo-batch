package com.example.demo;

import com.example.demo.proto.TransactionReferReply;
import com.example.demo.proto.TransactionReferRequest;
import com.example.demo.proto.TransactionReply;
import com.example.demo.proto.TransactionRequest;
import com.example.demo.support.MySQLTestContainer;
import com.example.demo.support.grpc.MockGrpcServer;
import com.example.demo.support.grpc.stub.TransactionStub;
import org.assertj.core.api.Assertions;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.csv.CsvURLDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@EnabledIfSystemProperty(named = "spring.profiles.active", matches = "mysql")
@DisabledIfSystemProperty(named = "spring.profiles.active", matches = "(?!mysql)")
@ActiveProfiles("mysql")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = IntegrationTestContext.class, initializers = ConfigDataApplicationContextInitializer.class)
class DemoBatchApplicationIT {

  static {
    MySQLTestContainer.initialize();
  }

  static MockGrpcServer server;

  @Autowired
  IDatabaseTester databaseTester;

  @BeforeAll
  static void startupServer() throws Exception {
    server = new MockGrpcServer(0, TransactionStub.INSTANCE);
    server.start();
    MySQLTestContainer.exportJdbcUrlToSystemProperty();
  }

  @AfterAll
  static void shutdownServer() {
    Optional.ofNullable(server).ifPresent(MockGrpcServer::stop);
    TransactionStub.INSTANCE.clear();
    MySQLTestContainer.clearJdbcUrlFromSystemProperty();
  }

  @AfterEach
  void tearDownDatabase() throws Exception {
    databaseTester.onTearDown();
  }

  @Test
  void runJob001() throws Exception {

    databaseTester.setDataSet(new FlatXmlDataSetBuilder().build(new ClassPathResource("db/xml/Before.xml").getInputStream()));
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.onSetup();

    String tranId = UUID.randomUUID().toString();
    // for create transaction
    TransactionStub.INSTANCE.mockOperationsForCreate
        .add(request -> TransactionReply.newBuilder().setId(tranId).build());
    // for refer transaction
    TransactionStub.INSTANCE.mockOperationsForRefer
        .add(request -> TransactionReferReply.newBuilder()
            .setId(tranId)
            .setName("Electric bil at March 2021")
            .setVendor("Demo Pay")
            .setAmount(5000)
            .setStatus(TransactionReferReply.Status.COMPLETED)
            .build());

    ProcessResult result = exe("--user=kazuki");

    Assertions.assertThat(result.exitCode).isEqualTo(0);
    Assertions.assertThat(result.stdOut).contains("New Transaction Id: " + tranId);
    Assertions.assertThat(result.stdOut).contains("Transaction Status of " + tranId + ": COMPLETED");

    // assert database
    {
      IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new ClassPathResource("db/xml/After.xml").getInputStream());
      ITable expectedTable = new SortedTable(expectedDataSet.getTable("t_users"));
      IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();
      ITable actualTable = new SortedTable(databaseDataSet.getTable("t_users"));
      Assertion.assertEquals(expectedTable, actualTable);
    }
    // assert request parameter of updating API on gRPC
    {
      TransactionRequest request = TransactionStub.INSTANCE.mockOperationsForCreate.pollInput();
      Assertions.assertThat(request.getName()).isEqualTo("Electric bil at March 2021");
      Assertions.assertThat(request.getVendor()).isEqualTo("Demo Pay");
      Assertions.assertThat(request.getAmount()).isEqualTo(5000);
    }
    // assert request parameter of referencing API on gRPC
    {
      TransactionReferRequest request = TransactionStub.INSTANCE.mockOperationsForRefer.pollInput();
      Assertions.assertThat(request.getId()).isEqualTo(tranId);
    }

  }

  @Test
  void runJob002() throws Exception {

    databaseTester.setDataSet(new CsvURLDataSet(new ClassPathResource("db/csv/before/").getURL()));
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.onSetup();

    String tranId = UUID.randomUUID().toString();
    // for create transaction
    TransactionStub.INSTANCE.mockOperationsForCreate
        .add(request -> TransactionReply.newBuilder().setId(tranId).build());
    // for refer transaction
    TransactionStub.INSTANCE.mockOperationsForRefer
        .add(request -> TransactionReferReply.newBuilder()
            .setId(tranId)
            .setName("Electric bil at March 2021")
            .setVendor("Demo Pay")
            .setAmount(5000)
            .setStatus(TransactionReferReply.Status.COMPLETED)
            .build());

    ProcessResult result = exe("--user=kazuki43zoo");

    Assertions.assertThat(result.exitCode).isEqualTo(0);
    Assertions.assertThat(result.stdOut).contains("New Transaction Id: " + tranId);
    Assertions.assertThat(result.stdOut).contains("Transaction Status of " + tranId + ": COMPLETED");

    // assert database
    {
      IDataSet expectedDataSet = new CsvURLDataSet(new ClassPathResource("db/csv/after/").getURL());
      ITable expectedTable = new SortedTable(expectedDataSet.getTable("t_users"));
      IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();
      ITable actualTable = new SortedTable(databaseDataSet.getTable("t_users"));
      Assertion.assertEquals(expectedTable, actualTable);
    }
    // assert request parameter of updating API on gRPC
    {
      TransactionRequest request = TransactionStub.INSTANCE.mockOperationsForCreate.pollInput();
      Assertions.assertThat(request.getName()).isEqualTo("Electric bil at March 2021");
      Assertions.assertThat(request.getVendor()).isEqualTo("Demo Pay");
      Assertions.assertThat(request.getAmount()).isEqualTo(5000);
    }
    // assert request parameter of referencing API on gRPC
    {
      TransactionReferRequest request = TransactionStub.INSTANCE.mockOperationsForRefer.pollInput();
      Assertions.assertThat(request.getId()).isEqualTo(tranId);
    }

  }

  @Test
  void runJob003() throws Exception {

    databaseTester.setDataSet(new XlsDataSet(new ClassPathResource("db/excel/Before.xlsx").getInputStream()));
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.onSetup();

    String tranId = UUID.randomUUID().toString();
    // for create transaction
    TransactionStub.INSTANCE.mockOperationsForCreate
        .add(request -> TransactionReply.newBuilder().setId(tranId).build());
    // for refer transaction
    TransactionStub.INSTANCE.mockOperationsForRefer
        .add(request -> TransactionReferReply.newBuilder()
            .setId(tranId)
            .setName("Electric bil at March 2021")
            .setVendor("Demo Pay")
            .setAmount(5000)
            .setStatus(TransactionReferReply.Status.COMPLETED)
            .build());

    ProcessResult result = exe("--user=kazuki43zoo");

    Assertions.assertThat(result.exitCode).isEqualTo(0);
    Assertions.assertThat(result.stdOut).contains("New Transaction Id: " + tranId);
    Assertions.assertThat(result.stdOut).contains("Transaction Status of " + tranId + ": COMPLETED");

    // assert database
    {
      IDataSet expectedDataSet = new XlsDataSet(new ClassPathResource("db/excel/After.xlsx").getInputStream());
      ITable expectedTable = new SortedTable(expectedDataSet.getTable("t_users"));
      IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();
      ITable actualTable = new SortedTable(databaseDataSet.getTable("t_users"));
      Assertion.assertEquals(expectedTable, actualTable);
    }
    // assert request parameter of updating API on gRPC
    {
      TransactionRequest request = TransactionStub.INSTANCE.mockOperationsForCreate.pollInput();
      Assertions.assertThat(request.getName()).isEqualTo("Electric bil at March 2021");
      Assertions.assertThat(request.getVendor()).isEqualTo("Demo Pay");
      Assertions.assertThat(request.getAmount()).isEqualTo(5000);
    }
    // assert request parameter of referencing API on gRPC
    {
      TransactionReferRequest request = TransactionStub.INSTANCE.mockOperationsForRefer.pollInput();
      Assertions.assertThat(request.getId()).isEqualTo(tranId);
    }

  }

  @Test
  void runJob004() throws Exception {

    TransactionStub.INSTANCE.mockOperationsForCreate
        .add(request -> {
          throw new RuntimeException("error test!!");
        });

    ProcessResult result = exe();

    Assertions.assertThat(result.exitCode).isEqualTo(1);

  }

  private ProcessResult exe(String... additionalArgs) throws IOException, InterruptedException {
    ProcessResult result = new ProcessResult();
    List<String> args = new ArrayList<>(Arrays.asList("java",
        "-Dspring.profiles.active=mysql",
        "-Ddb.host=" + System.getProperty("db.host", "localhost"),
        "-jar",
        "target/demo-batch.jar",
        "--spring.datasource.initialization-mode=never",
        "--demo.grpc.port=" + server.getPort()));
    String jdbcUrl = MySQLTestContainer.getJdbcUrl();
    if (jdbcUrl != null) {
      args.add("--spring.datasource.url=" + jdbcUrl);
    }
    args.addAll(Arrays.asList(additionalArgs));
    ProcessBuilder processBuilder = new ProcessBuilder(args);
    processBuilder.redirectErrorStream(true);
    Process process = processBuilder.start();
    result.exitCode = process.waitFor();
    try (BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.defaultCharset()))) {
      String line;
      while ((line = r.readLine()) != null) {
        result.stdOut.append(line).append(System.lineSeparator());
      }
    }
    System.out.println(result.stdOut);
    return result;
  }

  static class ProcessResult {
    private int exitCode;
    private final StringBuilder stdOut = new StringBuilder();
  }

}
