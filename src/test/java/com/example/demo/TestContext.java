package com.example.demo;

import com.example.demo.support.ExitCodeEventListener;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

public class TestContext {
  @Bean
  IDatabaseTester databaseTester(DataSource dataSource) {
    return new DataSourceDatabaseTester(new TransactionAwareDataSourceProxy(dataSource));
  }

  @Bean
  ExitCodeEventListener exitCodeEventListener() {
    return new ExitCodeEventListener();
  }

}
