package com.example.demo;

import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@EnableConfigurationProperties(DataSourceProperties.class)
public class IntegrationTestContext {

  @Bean
  DataSource dataSource(DataSourceProperties properties) {
    return DataSourceBuilder.create()
        .url(properties.getUrl())
        .username(properties.getUsername())
        .password(properties.getPassword())
        .build();
  }

  @Bean
  IDatabaseTester databaseTester(DataSource dataSource) {
    return new DataSourceDatabaseTester(dataSource);
  }

}
