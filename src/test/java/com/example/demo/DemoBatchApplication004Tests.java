package com.example.demo;

import com.example.demo.support.ExitCodeEventListener;
import com.example.demo.support.MySQLTestContainer;
import com.example.demo.support.TestDataSetupRunner;
import com.example.demo.support.grpc.MockGrpcServer;
import com.example.demo.support.grpc.stub.TransactionStub;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.Optional;

class DemoBatchApplication004Tests {

  private static MockGrpcServer server;

  @BeforeAll
  static void startupServer() throws Exception {
    server = new MockGrpcServer(0, TransactionStub.INSTANCE);
    server.setPortExportPropertyKey("demo.grpc.port");
    server.start();
    MySQLTestContainer.start();
    MySQLTestContainer.exportJdbcUrlToSystemProperty();
  }

  @AfterAll
  static void shutdownServer() {
    Optional.ofNullable(server).ifPresent(MockGrpcServer::stop);
    TransactionStub.INSTANCE.clear();
    MySQLTestContainer.clearJdbcUrlFromSystemProperty();
  }

  @Test
  void runJob() {
    try {
      SpringApplication.run(new Class[]{DemoBatchApplication.class, LocalTestContext.class}, new String[]{});
      Assertions.fail("");
    } catch (IllegalStateException e) {
      Assertions.assertThat(ExitCodeEventListener.isNormal()).isFalse();
      Assertions.assertThat(ExitCodeEventListener.event.getExitCode()).isEqualTo(1);
    }
  }

  @Import(TestContext.class)
  static class LocalTestContext {
    @Bean
    TestDataSetupRunner testDataSetupRunner() {
      return new TestDataSetupRunner(args -> {
        // ----------------------------
        // setup test data to gRPC stub
        // ----------------------------
        TransactionStub.INSTANCE.mockOperationsForCreate
            .add(request -> {
              throw new RuntimeException("error test!!");
            });
      });
    }

  }

}
