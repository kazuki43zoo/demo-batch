package com.example.demo;

import com.example.demo.support.ExitCodeEventListener;
import com.example.demo.support.MySQLTestContainer;
import com.example.demo.support.grpc.MockGrpcServer;
import com.example.demo.support.grpc.stub.TransactionStub;
import com.example.demo.proto.TransactionReferReply;
import com.example.demo.proto.TransactionReferRequest;
import com.example.demo.proto.TransactionReply;
import com.example.demo.proto.TransactionRequest;
import com.example.demo.support.TestDataSetupRunner;
import org.assertj.core.api.Assertions;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Optional;
import java.util.UUID;

@ExtendWith(OutputCaptureExtension.class)
@SpringBootTest(
    args = "--user=kazuki",
    classes = {DemoBatchApplication.class, DemoBatchApplication001Tests.LocalTestContext.class}
)
@ContextConfiguration(initializers = MySQLTestContainer.class)
class DemoBatchApplication001Tests {

  private static MockGrpcServer server;
  private static String tranId;

  @Autowired
  TestDataSetupRunner setupApplicationRunner;
  @Autowired
  IDatabaseTester databaseTester;

  @BeforeAll
  static void startupMockGrpcServer() throws Exception {
    server = new MockGrpcServer(0, TransactionStub.INSTANCE);
    server.setPortExportPropertyKey("demo.grpc.port");
    server.start();
  }

  @AfterAll
  static void shutdownMockGrpcServer() {
    Optional.ofNullable(server).ifPresent(MockGrpcServer::stop);
    TransactionStub.INSTANCE.clear();
  }

  @AfterEach
  void tearDownDatabase() throws Exception {
    setupApplicationRunner.rollbackTransaction();
    databaseTester.onTearDown();
  }

  @Test
  void runJob(CapturedOutput output) throws Exception {
    // assert exit code
    {
      Assertions.assertThat(ExitCodeEventListener.isNormal()).isTrue();
    }
    // assert database
    {
      IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new ClassPathResource("db/xml/After.xml").getInputStream());
      ITable expectedTable = new SortedTable(expectedDataSet.getTable("t_users"));
      IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();
      ITable actualTable = new SortedTable(databaseDataSet.getTable("t_users"));
      Assertion.assertEquals(expectedTable, actualTable);
    }
    // assert request parameter of updating API on gRPC
    {
      TransactionRequest request = TransactionStub.INSTANCE.mockOperationsForCreate.pollInput();
      Assertions.assertThat(request.getName()).isEqualTo("Electric bil at March 2021");
      Assertions.assertThat(request.getVendor()).isEqualTo("Demo Pay");
      Assertions.assertThat(request.getAmount()).isEqualTo(5000);
    }
    // assert request parameter of referencing API on gRPC
    {
      TransactionReferRequest request = TransactionStub.INSTANCE.mockOperationsForRefer.pollInput();
      Assertions.assertThat(request.getId()).isEqualTo(tranId);
    }
    // assert standard output
    {
      Assertions.assertThat(output).contains("New Transaction Id: " + tranId);
      Assertions.assertThat(output).contains("Transaction Status of " + tranId + ": COMPLETED");
    }
  }

  @Import(TestContext.class)
  static class LocalTestContext {
    @Bean
    TestDataSetupRunner testDataSetupRunner(PlatformTransactionManager transactionManager, IDatabaseTester databaseTester) {
      return new TestDataSetupRunner(transactionManager, args -> {
        // ----------------------------
        // insert test data to database
        // ----------------------------
        {
          databaseTester.setDataSet(new FlatXmlDataSetBuilder().build(new ClassPathResource("db/xml/Before.xml").getInputStream()));
          databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
          databaseTester.onSetup();
        }
        // ----------------------------
        // setup test data to gRPC stub
        // ----------------------------
        {
          tranId = UUID.randomUUID().toString();
          // for create transaction
          TransactionStub.INSTANCE.mockOperationsForCreate
              .add(request -> TransactionReply.newBuilder().setId(tranId).build());
          // for refer transaction
          TransactionStub.INSTANCE.mockOperationsForRefer
              .add(request -> TransactionReferReply.newBuilder()
                  .setId(tranId)
                  .setName("Electric bil at March 2021")
                  .setVendor("Demo Pay")
                  .setAmount(5000)
                  .setStatus(TransactionReferReply.Status.COMPLETED)
                  .build());
        }
      });
    }

  }

}
