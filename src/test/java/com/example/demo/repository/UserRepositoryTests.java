package com.example.demo.repository;

import com.example.demo.support.MySQLTestContainer;
import org.assertj.core.api.Assertions;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(initializers = MySQLTestContainer.class)
@Transactional
class UserRepositoryTests {

  @Autowired
  IDatabaseTester databaseTester;
  @Autowired
  UserRepository userRepository;

  @BeforeEach
  void setupDatabase() throws Exception {
    databaseTester.setDataSet(new FlatXmlDataSetBuilder().build(new ClassPathResource("db/xml/Before.xml").getInputStream()));
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.onSetup();
  }

  @AfterEach
  void tearDownDatabase() throws Exception {
    databaseTester.onTearDown();
  }

  @Test
  void findById() {
    User user = userRepository.findById("kazuki43zoo");
    Assertions.assertThat(user.id).isEqualTo("kazuki43zoo");
    Assertions.assertThat(user.name).isEqualTo("Kazuki Shimizu");
    Assertions.assertThat(user.email).isEqualTo("test@test.org");
  }

}
