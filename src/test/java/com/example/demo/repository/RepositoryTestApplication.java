package com.example.demo.repository;

import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

@SpringBootApplication
public class RepositoryTestApplication {
  @Bean
  IDatabaseTester databaseTester(DataSource dataSource) {
    return new DataSourceDatabaseTester(new TransactionAwareDataSourceProxy(dataSource));
  }
}
