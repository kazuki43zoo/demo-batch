package com.example.demo;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.boot.ExitCodeExceptionMapper;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;

@ConfigurationPropertiesScan
@SpringBootApplication
public class DemoBatchApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoBatchApplication.class, args);
  }

  @Bean
  ManagedChannel grpcManagedChannel(DemoBatchProperties properties) {
    return ManagedChannelBuilder.forAddress(properties.getGrpc().getHost(), properties.getGrpc().getPort())
        .usePlaintext()
        .build();
  }

  @Bean
  ExitCodeExceptionMapper exitCodeExceptionMapper() {
    return t -> t instanceof ExitCodeGenerator ? 0 : 1;
  }

}
