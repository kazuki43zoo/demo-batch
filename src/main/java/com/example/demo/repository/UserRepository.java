package com.example.demo.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserRepository {

  @Select("SELECT * FROM t_users WHERE id = #{id}")
  User findById(String id);

  @Insert("INSERT INTO t_users (id, name, email) VALUES(#{id}, #{name}, #{email})")
  void create(User user);

  @Update("UPDATE t_users SET name = #{name}, email = #{email} WHERE id = #{id}")
  void update(User user);

}
