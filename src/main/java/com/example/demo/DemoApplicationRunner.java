package com.example.demo;

import com.example.demo.client.Transaction;
import com.example.demo.client.TransactionApiClient;
import com.example.demo.repository.User;
import com.example.demo.repository.UserRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DemoApplicationRunner implements ApplicationRunner {

  private final UserRepository userRepository;
  private final TransactionApiClient transactionApiClient;

  public DemoApplicationRunner(UserRepository userRepository, TransactionApiClient transactionApiClient) {
    this.userRepository = userRepository;
    this.transactionApiClient = transactionApiClient;
  }

  @Transactional
  @Override
  public void run(ApplicationArguments args) {

    String userId = args.containsOption("user") ? args.getOptionValues("user").get(0) : "kazuki";

    User user = userRepository.findById(userId);

    if (user == null) {
      User newUser = new User();
      newUser.id = userId;
      newUser.name = "Kazuki Shimizu";
      newUser.email = "test@test.com";
      userRepository.create(newUser);
    } else {
      user.name = user.name + "(Update)";
      userRepository.update(user);
    }


    Transaction newTransaction = new Transaction();
    newTransaction.name = "Electric bil at March 2021";
    newTransaction.vendor = "Demo Pay";
    newTransaction.amount = 5000;
    transactionApiClient.create(newTransaction);
    System.out.println("New Transaction Id: " + newTransaction.id);

    Transaction transaction = transactionApiClient.findById(newTransaction.id);
    System.out.println("Transaction Status of " + newTransaction.id + ": " + transaction.status);
  }
}
