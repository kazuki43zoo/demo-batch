package com.example.demo.client;

public class Transaction {
  public String id;
  public String name;
  public String vendor;
  public long amount;
  public Status status;

  enum Status {
    ACCEPT,
    COMPLETED,
    REJECTED;
  }
}
