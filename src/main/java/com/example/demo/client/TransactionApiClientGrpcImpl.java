package com.example.demo.client;

import com.example.demo.proto.TransactionGrpc;
import com.example.demo.proto.TransactionReferReply;
import com.example.demo.proto.TransactionReferRequest;
import com.example.demo.proto.TransactionReply;
import com.example.demo.proto.TransactionRequest;
import io.grpc.ManagedChannel;
import org.springframework.stereotype.Component;

@Component
public class TransactionApiClientGrpcImpl implements TransactionApiClient {
  private final TransactionGrpc.TransactionBlockingStub blockingStub;

  TransactionApiClientGrpcImpl(ManagedChannel managedChannel) {
    this.blockingStub = TransactionGrpc.newBlockingStub(managedChannel);
  }

  @Override
  public void create(Transaction transaction) {
    TransactionRequest request = TransactionRequest.newBuilder()
        .setName(transaction.name)
        .setVendor(transaction.vendor)
        .setAmount(transaction.amount)
        .build();
    TransactionReply reply = blockingStub.create(request);
    transaction.id = reply.getId();
  }

  @Override
  public Transaction findById(String id) {
    TransactionReferRequest request = TransactionReferRequest.newBuilder()
        .setId(id)
        .build();
    TransactionReferReply reply = blockingStub.refer(request);
    Transaction transaction = new Transaction();
    transaction.id = reply.getId();
    transaction.name = reply.getName();
    transaction.vendor = reply.getVendor();
    transaction.status = Transaction.Status.valueOf(reply.getStatus().name());
    return transaction;
  }
}
