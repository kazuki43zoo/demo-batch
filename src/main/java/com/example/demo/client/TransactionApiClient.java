package com.example.demo.client;

public interface TransactionApiClient {

  void create(Transaction transaction);

  Transaction findById(String id);
}
