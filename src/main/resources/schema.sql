CREATE TABLE t_users (
    id varchar primary key,
    name varchar not null,
    email varchar not null,
    constraint uk_t_users unique key (email)
);